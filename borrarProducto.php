<?php
include('functions.php');

$id = $_GET['id'];
    if($id) {
      $product = getProduct($id);
          if($product) {
            $deleted = deleteProduct($id);
                if($deleted) {
                   header('Location: /Tienda/productos.php?status=success');
                } else {
                  header('Location: /Tienda/productos.php?status=error');
                }
          } else {
            header('Location: /Tienda/productos.php?status=error');
          }
    } else {
      header('Location: /Tienda/productos.php');
    }