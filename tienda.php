<?php
  require('functions.php');

  $message = "";
  if(!empty($_REQUEST['status'])) {
    switch($_REQUEST['status']) {
      case 'success':
        $message = 'User was added succesfully';
      break;
      case 'error':
        $message = 'There was a problem inserting the user';
      break;
    }
  }
?>



<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tienda</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/tiendadP.css">
    <link rel="shortcut icon" href="img/fIcon4.png">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  
</head>

<body >
    <main>
        <div class="content-all">
            <header> </header>
            <input type="checkbox" id="check">
            <label for="check" class="icon-menu">Categorias</label>
            
            <nav class="navbar navbar-expand-sm bg-pink navbar-light">
            <a class="navbar-brand" href="#">
                <img src="img/logo2.png" alt="Logo" style="width:50%;">
            </a>
            
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="iniciarSesion.php">| Iniciar Sesion |</a>
                </li>
                   <li class="nav-item">
                    <a class="nav-link" href="registro.php">| Registrarse |</a>
                </li>
                
            </ul>
        </nav>
            
           
            
            <nav class="menu">
                <ul>
                   
                    <li><a href="tienda.php">Inicio</a></li>
                    <li><a href="cat_blu.php">Blusas</a></li>
                    <li><a href="cat_pant.php">Pantalones</a></li>
                    <li><a href="cat_sho.php">Short</a></li>
                    <li><a href="cat_suet.php">Sueters</a></li>
                    <li><a href="cat_cam.php">Camisas</a></li>
                   
                </ul>
            </nav>
            <article>
              
                  <h1>Bienvenido a la tienda Online</h1>
              <table class="table table-light">
                  <tbody>
                    <tr>
                      <td>Producto</td>
                      <td>Nombre</td>
                      <td>Descripcion</td>
                      <td>Categoria</td>
                      <td>Stock</td>
                      <td>Precio</td>
                    </tr>
                    
                     
                      <?php
                       $producto = getproduct();
                       $productHtml = "";
                         foreach ($producto as $prod) {
                           $productHtml .= 
                                "<tr>
                                    <td><img src='{$prod['imagen']}' width='60%' height='15%'> </td>
                                    <td>{$prod['nombre']}</td>
                                    <td>{$prod['descripcion']}</td>
                                    <td>{$prod['categoria']}</td>
                                    <td>{$prod['stock']}</td>
                                    <td>{$prod['precio']}</td>
                                    <td><a href='' class='btn btn-primary' onclick=''>Carrito</a>
                                    </td>
                                </tr>";
                          }
                      echo $productHtml;
                    ?>
                  </tbody>
                </table>
            </article>
            
        </div>
    </main>
    </body>
</html>