<?php
  require('functions.php');
  

  $message = "";
  if(!empty($_REQUEST['status'])) {
    switch($_REQUEST['status']) {
      case 'success':
        $message = 'User was added succesfully';
      break;
      case 'error':
        $message = 'There was a problem inserting the user';
      break;
    }
  }
?>



<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tienda - Cliente</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/tiendadP.css">
    <link rel="shortcut icon" href="img/fIcon4.png">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  
</head>

<body >
    <main>
        <div class="content-all">
            <header> </header>
            <input type="checkbox" id="check">
            <label for="check" class="icon-menu">Menú Cliente</label>
            
           
            
            <nav class="menu">
                <ul>
                    <li class="menu__item"><a class="menu__link select" href="cliente.php">Perfil</a></li>
                    <li class="menu__item"><a class="menu__link" href="cerrarSesion.php">Cerrar Sesión </a></li>
                    <li class="menu__item"><a class="menu__link" href="tienda.php">Catalogo Productos </a></li>
                   
                </ul>
            </nav>
            <article>
               
               
                
                                  <h3>Productos Adquiridos</h3>
                                   <table class="table table-light">
                                      <tbody>
                                        <tr>
                                          <td>Cantidad</td>
                                        </tr>
                                         <?php
                                          $countUs = countUser();
                                          $cUsHtml = "";
                                          foreach ($countUs as $cu) {
                                            $cUsHtml .= 
                                            "<tr>
                                            <td>{$cu}</td>
                                            </tr>";
                                          }
                                          echo $cUsHtml;
                                        ?>

                                      </tbody>
                                </table>
                                
                             
                                  <h3>Total Productos Adquiridos</h3>
                                   <table class="table table-light">
                                      <tbody>
                                        <tr>
                                          <td>Cantidad</td>
                                        </tr>
                                         <?php
                                          $countUs = countSale();
                                          $cUsHtml = "";
                                          foreach ($countUs as $cu) {
                                            $cUsHtml .= 
                                            "<tr>
                                            <td>{$cu}</td>
                                            </tr>";
                                          }
                                          echo $cUsHtml;
                                        ?>

                                      </tbody>
                                </table>
                                
                              
                                  <?php
                                      session_start();

                                      $user = $_SESSION['user'];
                                      if (!$user) {
                                        header('Location: /Tienda/tienda.php');
                                      }
                                      ?>

                                      <h1><?php echo $user['cedula'] ?>, Bienvenido a la tienda My Trees </h1>
                                      
                                  <h3>Compras Realizadas</h3>
                                   
                                    <table class="table table-light">
                                      <tbody>
                                        <tr>
                                          <td>Nombre</td>
                                          <td>Precio</td>
                                          <td>Fecha</td>
                                    
                                        </tr>
                                         <?php
   
                                          $purch = getPurchaseClient($user['cedula']);
                                          $productHtml = "";
                                          foreach ($purch as $client) {
                                            $productHtml .= 
                                            "<tr 
                                            <td>{$client['nombreA']}</td>
                                            <td>{$client['precio']}</td>
                                            <td>{$client['fecha']}</td>
                                            </tr>";
                                          }
                                          echo $productHtml;     
                                         
                                        ?>
                                      </tbody>
                                </table>
            </article>
            
        </div>
    </main>
    </body>
</html>