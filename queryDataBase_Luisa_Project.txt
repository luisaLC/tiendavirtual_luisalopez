create table usuario(
 id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 nombre varchar(100) not null,
 apellido varchar(100) not null, 
 telefono int not null,
 direccion varchar(100) not null,
 correo varchar(100) not null,
 contrasena varchar(100) not null,
 pais varchar(100),
 role varchar(100)
);

ALTER TABLE usuario
  ADD cedula VARCHAR(50);

create table lista(
 id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 nombre varchar(100) not null,
 categoria varchar(100) not null
);

CREATE TABLE producto (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  nombre varchar(100) NOT NULL,
  descripcion varchar(100) NOT NULL,
  imagen varchar(150) NOT NULL,
  categoria varchar(100) NOT NULL,
  stock int(255) NOT NULL,
  precio int(255) NOT NULL
);

CREATE TABLE ventas(
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  nombreC varchar(100) NOT NULL,
  nombreA varchar(100) NOT NULL,
  precio int(255) NOT NULL,
  fechaCompra DATE
  );




inser into ventas(nombreC, nombreA, precio, fecha) values('','',,)
inser into ventas(nombreC, nombreA, precio, fecha) values('','',,)
insert into ventas(nombreC, nombreA, precio, fechaCompra) values('700800887','Camisa botones', 6500 ,0000-00-00);

insert into usuario (nombre, apellido, telefono, direccion, correo, contrasena, pais, role) values
('Juliana', 'Medina', 84154845,'La Fortuna','julim@corderohotmail.com', '1234', 'Costa Rica', 'administrador');

insert into usuario (nombre, apellido, telefono, direccion, correo, contrasena, pais, role) values
('Fainier', 'López', 84154845,'La Fortuna','fanil@corderohotmail.com', '1234', 'Costa Rica', 'administrador');

insert into usuario (nombre, apellido, telefono, direccion, correo, contrasena, pais, role) values('Naza', 'Salas', 84154845,'La Fortuna','nazaSalas@corderohotmail.com', '1234', 'Costa Rica', 'administrador');

insert into usuario (nombre, apellido, telefono, direccion, correo, contrasena, pais, role) values
('Melissa', 'Rudock', 84154845,'La Fortuna','melRud@corderohotmail.com', '1234', 'Costa Rica', 'administrador');

insert into lista (nombre, categoria) values('Blusa', 'tirantes');
insert into lista (nombre, categoria) values('Blusa', 'Manga Larga');
insert into lista (nombre, categoria) values('Blusa', 'Manga Corta');

insert into producto (nombre, descripcion, imagen, categoria, stock, precio) values('Camisa botones', 'Blusa blanca tirantes con estampados de estrellas', 'img', 'CAM', 40, 5600);

insert into producto (nombre, descripcion, imagen, categoria, stock, precio) values('Blusa Manga larga', 'Blusa amarilla con estampados de estrellas', 'img', 'BLU', 50, 5600);

insert into producto (nombre, descripcion, imagen, categoria, stock, precio) values('Short corto', 'Blusa blanca tirantes con estampados de estrellas', 'img', 'SHO', 40, 5600);

insert into producto (nombre, descripcion, imagen, categoria, stock, precio) values('Pantalon LEVIS', 'Blusa amarilla con estampados de estrellas', 'img', 'PANT', 50, 5600);

insert into producto (nombre, descripcion, imagen, categoria, stock, precio) values('Sueter floreado', 'Blusa amarilla con estampados de estrellas', 'img', 'SUET', 50, 5600);

