<?php
  require('functions.php');

  $message = "";
  if(!empty($_REQUEST['status'])) {
    switch($_REQUEST['status']) {
      case 'success':
        $message = 'User was added succesfully';
      break;
      case 'error':
        $message = 'There was a problem inserting the user';
      break;
    }
  }
?>



<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tienda - Categorias</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/tiendadP.css">
    <link rel="shortcut icon" href="img/fIcon4.png">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  
</head>

<body >
    <main>
        <div class="content-all">
            <header> </header>
            <input type="checkbox" id="check">
            <label for="check" class="icon-menu">Menú</label>
            
           
            
            <nav class="menu">
                <ul>
                     <li class="menu__item"><a class="menu__link select" href="administrador.php">Inicio</a></li>
				<li class="menu__item"><a class="menu__link" href="categorias.php">Categorias</a></li>
				<li class="menu__item"><a class="menu__link" href="productos.php">Productos</a></li>
				<li class="menu__item"><a class="menu__link" href="cerrarSesion.php">Cerrar Sesión </a></li>
                <li class="menu__item"><a class="menu__link" href="tienda.php">Catalogo Productos </a></li>
                   
                </ul>
            </nav>
            <article>
                <div class="formuario">
                               <form action="/Tienda/crearLista.php" onsubmit="return validateStudentForm();" method="POST" class="form-inline" role="form">
                               <h3>Agregar Categorias:    </h3>
                               <br><br>
                                <input class="input" type="text" id="nombre" name="nombre" placeholder=" Nombre " required autofocus>
                                <br><br>
                                <input class="input" type="text" id="categoria" name="categoria" placeholder=" Categoria " required autofocus>
                                
                                   <div class="btn__form">
                                     <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </form>
                               </div>
                                
                                 <br><br>
                                  <h3>Categorias</h3>
                                  <table class="table table-light">
                                      <tbody>
                                        <tr>
                                          <td>Nombre</td>
                                          <td>Categoria</td>
                                        </tr>
                                         <?php
                                          $lista = getlist();
                                          $listHtml = "";
                                          foreach ($lista as $list) {
                                            $listHtml .= 
                                            "<tr 
                                            id='list_{$list['id']}'>
                                            <td>{$list['nombre']}</td>
                                            <td>{$list['categoria']}</td>
                                            <td><a href='borrarLista.php?id={$list['id']}' class='btn btn-primary' onclick='borrarLista.php({$list['id']})'>Delete</a>
                                            <td><a href='borrarLista.php?id={$list['id']}' class='btn btn-primary' onclick='borrarLista.php({$list['id']})'>Editar</a>
                                            </td>
                                            </tr>";
                                          }
                                          echo $listHtml;
                                        ?>

                                      </tbody>
                                </table>
                       
            </article>
            
        </div>
    </main>
    </body>
</html>