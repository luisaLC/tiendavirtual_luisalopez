<?php
  require('functions.php');
  $mysqli = new mysqli('localhost:3306', 'root', 'lulitrix', 'tienda');

  $message = "";
  if(!empty($_REQUEST['status'])) {
    switch($_REQUEST['status']) {
      case 'success':
        $message = 'User was added succesfully';
      break;
      case 'error':
        $message = 'There was a problem inserting the user';
      break;
    }
  }
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tienda - Productos</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/tiendadP.css">
    <link rel="shortcut icon" href="img/fIcon4.png">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  
</head>

<body >
    <main>
        <div class="content-all">
            <header> </header>
            <input type="checkbox" id="check">
            <label for="check" class="icon-menu">Menú</label>
            
           
            
            <nav class="menu">
                <ul>
                     <li class="menu__item"><a class="menu__link select" href="administrador.php">Inicio</a></li>
				<li class="menu__item"><a class="menu__link" href="categorias.php">Categorias</a></li>
				<li class="menu__item"><a class="menu__link" href="productos.php">Productos</a></li>
				<li class="menu__item"><a class="menu__link" href="cerrarSesion.php">Cerrar Sesión </a></li>
                <li class="menu__item"><a class="menu__link" href="tienda.php">Catalogo Productos </a></li>
                </ul>
            </nav>
            <article>
                <form action=" /Tienda/crearProducto.php" onsubmit="return validateStudentForm();" method="POST" class="form-inline" role="form">
                               <h3>Guardar Productos</h3>
                                <input class="input" type="text" id="nombre" name="nombre" placeholder=" Nombre " required autofocus>
                                <input class="input" type="text" id="descripcion" name="descripcion"  placeholder=" Descripción" required autofocus>
                               
                                <input class="input" type="number" id="stock" name="stock"  placeholder=" Stock" required autofocus>
                                <input class="input" type="text" id="precio" name="precio"  placeholder=" Precio" required autofocus>
                                
                                 <input type="file" name="imagen" id="picture">
                                
                               <select type="text" id="precio" name="categoria">
                                    <option value="0">Seleccione:</option>
                                    <?php
                                      $query = $mysqli -> query ("SELECT * FROM lista");
                                      while ($valores = mysqli_fetch_array($query)) {
                                        echo '<option value="'.$valores[categoria].'">'.$valores[categoria].'</option>';
                                      }
                                    ?>
                                </select>
                               
                                   <div class="btn__form">
                                     <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </form>
                              
                                 <br><br>
                                  <h3>Productos</h3>
                                  <table class="table table-light">
                                      <tbody>
                                        <tr>
                                          <td>Nombre</td>
                                          <td>Descripcion</td>
                                          <td>Imagen</td>
                                          <td>Categoria</td>
                                           <td>Stock</td>
                                          <td>Precio</td>
                                        </tr>
                                         <?php
                                          $producto = getproduct();
                                          $productHtml = "";
                                          foreach ($producto as $prod) {
                                            $productHtml .= 
                                            "<tr 
                                            id='product_{$prod['id']}'>
                                            <td>{$prod['nombre']}</td>
                                            <td>{$prod['descripcion']}</td>
                                            <td>{$prod['imagen']}</td>
                                            <td>{$prod['categoria']}</td>
                                            <td>{$prod['stock']}</td>
                                            <td>{$prod['precio']}</td>
                                            <td><a href='borrarProducto.php?id={$prod['id']}' class='btn btn-primary' onclick='borrarProducto.php({$prod['id']})'>Eliminar</a>
                                            </td>
                                            <td><a href='borrarProducto.php?id={$prod['id']}' class='btn btn-primary' onclick='borrarProducto.php({$prod['id']})'>Editar</a>
                                            </tr>";
                                          }
                                          echo $productHtml;
                                        ?>
                                      </tbody>
                                </table>
                       
            </article>
            
        </div>
    </main>
    </body>
</html>