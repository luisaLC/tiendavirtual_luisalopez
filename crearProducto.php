<?php
  include('functions.php');

  if(isset($_POST['nombre']) && isset($_POST['descripcion']) && isset($_POST['imagen']) 
    && isset($_POST['categoria']) && isset($_POST['stock'])
    && isset($_POST['precio'])) {
    $saved = saveProduct($_POST);
    

    if($saved) {
      header('Location: /Tienda/productos.php?status=success');
    } else {
      header('Location: /Tienda/productos.php?status=error');
    }
  } else {
    header('Location: /Tienda/productos.php?status=error');
  }