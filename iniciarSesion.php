<?php
  session_start();
  if ($_SESSION && $_SESSION['user']){
    //user already logged in
    header('Location: /Tienda/administrador.php');
  }

  $message = "";
  if(!empty($_REQUEST['status'])) {
    switch($_REQUEST['status']) {
      case 'login':
        $message = 'User does not exists';
      break;
      case 'error':
        $message = 'There was a problem inserting the user';
      break;
    }
  }
?>



<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Iniciar Sesión</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/iniciarSesion.css">
    <link rel="shortcut icon" href="img/fIcon4.png">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>

<body >
   <body >
   
    <div class="contenedor">
        <nav class="navbar navbar-expand-sm bg-pink navbar-light">
            <a class="navbar-brand" href="#">
                <img src="img/logo2.png" alt="Logo" style="width:50%;">
            </a>
            
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="tienda.php">| Inicio |</a>
                </li>
                   <li class="nav-item">
                    <a class="nav-link" href="registro.php">| Registrarse |</a>
                </li>
                
            </ul>
        </nav>
    </div>
    <div class="container mt-3">
        <div class="d-flex mb-3">
            <div class="p2 flex-fill">
                <div class="row">
                   
                    <div class="col-sm">
                        <div class="cuentas">
                               <BR></BR>
                            <h2> <a>¡La mejor tienda Online! </a> </h2>
                                <p>
                                 Ropa de primera y segunda mano, seleccionada con la mejor calidad que puedas encontrar.
                               </p>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="formuario">
                             <form  action="/Tienda/autenticarUsuario.php" method="POST" class="form-inline" role="form" >
                                   <h2>Inicio de Sesión</h2>
                                        <input class="input" type="text" id="correo" name="correo" placeholder=" Usuario " >
                                         <input class="input" type="password" id="contrasena" name="contrasena" placeholder=" Contraseña" >
                                         <br>
                                         <h1> </h1>
                                         <br>
                                         <h4>No tienes cuenta?  <a href="registrarse.php" id="reg"> Registrarse </a></h4>
                                         <h1> </h1>
                                         <div class="btn__form">
                                            <button type="submit" class="btn btn-primary">Iniciar Sesión</button>
                                         </div>
                                         <h1> </h1>
                                    </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
      

   
</body>

</html>