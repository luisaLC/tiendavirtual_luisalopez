<?php
include('functions.php');

$id = $_GET['id'];
if($id) {
  $list = getList($id);
  if($list) {
    $deleted = deleteList($id);
    if($deleted) {
      header('Location: /Tienda/categorias.php?status=success');
    } else {
      header('Location: /Tienda/categorias.php?status=error');
    }
  } else {
    header('Location: /Tienda/categorias.php?status=error');
  }
} else {
  header('Location: /Tienda/categorias.php');
}