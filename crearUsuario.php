<?php
  include('functions.php');

  if(isset($_POST['nombre']) && isset($_POST['apellido']) && isset($_POST['cedula']) && isset($_POST['telefono']) 
    && isset($_POST['correo']) && isset($_POST['direccion'])
    && isset($_POST['pais']) && isset($_POST['contrasena'])) {
    $saved = saveUser($_POST);
    

    if($saved) {
      header('Location: /Tienda/registro.php?status=success');
    } else {
      header('Location: /Tienda/tienda.php?status=error');
    }
  } else {
    header('Location: /Tienda/tienda.php?status=error');
  }