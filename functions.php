<?php
/**
 *  Gets a new mysql connection
 */
function getConnection() {
  $connection = new mysqli('localhost:3306', 'root', 'lulitrix', 'tienda');
  if ($connection->connect_errno) {
    printf("Connect failed: %s\n", $connection->connect_error);
    die;
  }
  return $connection;
}

/**
 * Inserts a new user, list and products to the database
 *
 * @user @list @product An associative array with the user, list and product information
 */

function saveUser($user) {
  $conn = getConnection();
  $sql = "INSERT INTO usuario( `nombre`, `apellido`, `cedula` , `telefono` , `correo`, `direccion`, `pais`, `contrasena` )
          VALUES ('{$user['nombre']}', '{$user['apellido']}', '{$user['cedula']}','{$user['telefono']}',
                  '{$user['correo']}', '{$user['direccion']}','{$user['pais']}',
                  '{$user['contrasena']}')";
  $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return true;
}

function saveVentas($vent) {
  $conn = getConnection();
  $sql = "INSERT INTO ventas( `nombreC`, `nombreA`, `precio` , `fechaCompra`)
          VALUES ('{$vent['nombreC']}', '{$vent['nombreA']}','{$vent['precio']}',
                  '{$vent['fechaCompra']})";
  $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return true;
}

function savelist($list) {
  $conn = getConnection();
  $sql = "INSERT INTO lista( `nombre`, `categoria`)
          VALUES ('{$list['nombre']}', '{$list['categoria']}')";
  $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return true;
}

function saveProduct($product) {
  $conn = getConnection();
  $sql = "INSERT INTO producto( `nombre`, `descripcion`, `imagen` , `categoria`, `stock`, `precio`)
          VALUES ('{$product['nombre']}', '{$product['descripcion']}','{$product['imagen']}',
                  '{$product['categoria']}', '{$product['stock']}','{$product['precio']}')";
  $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return true;
}


/**
 * Updates an existing user, list, product to the database
 *
 * @user @list @product An associative array with the student information
 */
function updateUser($user) {
  $conn = getConnection();
  $sql = "UPDATE usuario set `nombre` = '{$user['nombre']}', `apellido` = '{$user['apellido']}',
    `correo` = '{$user['correo']}',`telefono` = '{$user['telefono']}',`direccion` = '{$user['direccion']}' ,`correo` = '{$user['correo']}' ,`contrasena` = '{$user['contrasena']}' WHERE `id`= {$user['id']}";

  $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return true;
}

function updateList($list) {
  $conn = getConnection();
  $sql = "UPDATE lista set `nombre` = '{$list['nombre']}', `categoria` = '{$list['categoria']}' WHERE `id`= {$student['id']}";

  $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return true;
}

function updateProduct($product) {
  $conn = getConnection();
  $sql = "UPDATE producto set `nombre` = '{$product['nombre']}', `descripcion` = '{$product['descripcion']}',
    `imagen` = '{$product['imagen']}',`categoria` = '{$product['categoria']}',`stock` = '{$product['stock']}' ,`precio` = '{$product['prcio']}' WHERE `id`= {$product['id']}";

  $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return true;
}

/**
 * Get all user, list, product from the database
 *
 */
function getUser(){
  $conn = getConnection();
  $sql = "SELECT * FROM usuario ";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result;
}

function getList(){
  $conn = getConnection();
  $sql = "SELECT * FROM lista";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result;
}

function getProduct(){
  $conn = getConnection();
  $sql = "SELECT * FROM producto";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result;
}

/**
 * Get all PRODUCTS BY CATEGORIA,  from the database
 *
 */

function getProductSHO(){
  $conn = getConnection();
  $sql = "SELECT * FROM producto WHERE categoria = 'SHO'";
     
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result;
}

function getProductBLU(){
  $conn = getConnection();
  $sql = "SELECT * FROM producto WHERE categoria = 'BLU'";
     
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result;
}

function getProductPANT(){
  $conn = getConnection();
  $sql = "SELECT * FROM producto WHERE categoria = 'PANT'";
     
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result;
}

function getProductSUET(){
  $conn = getConnection();
  $sql = "SELECT * FROM producto WHERE categoria = 'SUET'";
     
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result;
}

function getProductCAM(){
  $conn = getConnection();
  $sql = "SELECT * FROM producto WHERE categoria = 'CAM'";
     
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result;
}

/**
 * Get all products BY clients,  from the database
 *
 */
function getPriceClient($client){
  $conn = getConnection();
  $sql = "SELECT SUM(precio) FROM ventas WHERE `nombreC`= $client";
     
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result;
}

function getSumClient($client){
  $conn = getConnection();
  $sql = "SELECT * FROM ventas WHERE `nombreC`= $client";
     
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result;
}




/**
 * Deletes an user, list, product from the database
 */
function deleteUser($id){
  $conn = getConnection();
  $sql = "DELETE FROM usuario WHERE id = $id";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return true;
}

function deleteList($id){
  $conn = getConnection();
  $sql = "DELETE FROM lista WHERE id = $id";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return true;
}

function deleteProduct($id){
  $conn = getConnection();
  $sql = "DELETE FROM producto WHERE id = $id";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return true;
}

/**
 * Get one specific student from the database
 *
 * @id Id of the student
 */
function getArbol($id){
   $conn = getConnection();
  $sql = "SELECT * FROM tiendaArboles WHERE id = $id";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result->fetch_array();
}


/**
 * Get one specific user from the database
 *
 * @id Id of the student
 */
function authenticate($correo, $contrasena){
  $conn = getConnection();
  $sql = "SELECT * FROM usuario WHERE `correo` = '$correo' AND `contrasena` = '$contrasena'";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return $result->fetch_array();
  
}

/**
 * Get one specific user from the database
 *
 * @id Id of the student
 */
function countUser(){
  $conn = getConnection();
  $sql = "SELECT count(*) FROM usuario";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return $result->fetch_array();
  
}

function countSale(){
  $conn = getConnection();
  $sql = "SELECT count(*) FROM ventas";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return $result->fetch_array();
  
}


function countPrice(){
  $conn = getConnection();
  $sql = "SELECT SUM(precio) FROM ventas";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return $result->fetch_array();
  
}



/**
 * Uploads an image to the server
 *
 * @inputName name of the input that holds the image in the request
 */
function uploadPicture($inputName){
  $fileObject = $_FILES[$inputName];

  $target_dir = "img/";
  $target_file = $target_dir . basename($fileObject["name"]);
  $uploadOk = 0;
  if (move_uploaded_file($fileObject["tmp_name"], $target_file)) {
    return $target_file;
  } else {
    return false;
  }
}

