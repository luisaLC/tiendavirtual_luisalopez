<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	 <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registrar Usuario</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="shortcut icon" href="img/fIcon4.png">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="js/bootstrap.min.js"></script> 
	<link rel="stylesheet" href="css/prod.css">
	

</head>
   
<body>
     <div class="contenedor">
        <nav class="navbar navbar-expand-sm bg-pink navbar-light">
            <a class="navbar-brand" href="#">
                <img src="img/logo2.png" alt="Logo" style="width:50%;">
            </a>
            
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="iniciarSesion.php">| Iniciar Sesion |</a>
                </li>
                   <li class="nav-item">
                    <a class="nav-link" href="registrarse.php">| Registrarse |</a>
                </li>
                
            </ul>
        </nav>
    </div>
    
    <header class="header">
		<div class="contenedor">
			<h1 class="logo"></h1>
            <span class="icon-menu" id="btn-menu"></span>
            <nav class="nav" id="nav">
				<ul class="menu">
				<li class="menu__item"><a class="menu__link" href="administrador.php">Inicio</a></li>
				<li class="menu__item"><a class="menu__link" href="categorias.php">Categorias</a></li>
				<li class="menu__item"><a class="menu__link " href="productos.php">Productos</a></li>
				<li class="menu__item"><a class="menu__link select" href="usuarios.php">Usuarios</a></li>
				<li class="menu__item"><a class="menu__link" href="">Cerrar Sesión </a></li>
				</ul>
            </nav>
        </div>
	</header>
	
	  <div class="container mt-2">
        <div class="d-flex mb-3">
            <div class="p2 flex-fill">
                <div class="row">
                    <div class="col-sm">
                        <div class="cuentas">
                                 
                        <div class="formuario">
                              
                              
                                 <br><br>
                                  <h3>Usuarios</h3>
                                  <table class="table table-light">
                                      <tbody>
                                        <tr>
                                          <td>Nombre</td>
                                          <td>Apellido</td>
                                          <td>Telefono</td>
                                          <td>Correo</td>
                                          <td>Direccion</td>
                                        </tr>
                                         <?php
                                          $arboles = getArboles();
                                          $arbolesHtml = "";
                                          foreach ($arboles as $arbol) {
                                            $arbolesHtml .= 
                                            "<tr 
                                            id='arbol_{$arbol['id_arboles']}'>
                                            <td>{$arbol['nombre']}</td>
                                            <td>{$arbol['fecha']}</td>
                                            <td>{$arbol['detalles']}</td>
                                            <td>{$arbol['ubicacion']}</td>
                                            <td><a href='deleteStudent.php?id={$arbol['id_arboles']}' class='btn btn-primary' onclick='deleteStudent.php({$arbol['id_arboles']})'>Delete</a>
                                            </td>
                                            </tr>";
                                          }
                                          echo $arbolesHtml;
                                        ?>

                                      </tbody>
                                </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<article>
	      
                 
	</article>
	
    
    
	<script src="js/menu.js"></script>
</body>
</html>